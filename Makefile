clean:
	rm -rf data/ site/

virtualenv:
	python3 -m venv .venv

shell:
	. .venv/bin/activate

init: virtualenv shell

pip:
	python3 -m pip install -r requirements.txt

setup:
	python3 setup.py develop

install: pip setup

prettier:
	black lss/ && isort -rc lss/

test:
	export PYTHONPATH=`pwd` && \
	mypy lss/__main__.py && \
	pytest --doctest-modules --cov=lss tests/ lss/ \
	&& coverage-badge -fo .coverage.svg

fixture1:
	mkdir -p data/f1.4k && \
	seq -f "data/f1.4k/v001.%04g.txt" 4000 | xargs touch

fixture2:
	mkdir -p data/f2.missing && \
	seq -f "data/f2.missing/v001.%04g.txt" 1000 | xargs touch && \
	seq -f "data/f2.missing/v001.%04g.txt" 1003 1100 | xargs touch

fixture3:
	mkdir -p data/f3 && \
	seq -f "data/f3/v001.%04g.txt" 3 | xargs touch

fixture4:
	mkdir -p data/f4 && \
	seq -f "data/f4/sd_fx29.%04g.rgb" 101 121 | xargs touch && \
	seq -f "data/f4/sd_fx29.%04g.rgb" 123 147 | xargs touch && \
	touch data/f4/elem.info && touch data/f4/strange.xml

fixture5:
	mkdir -p data/f5 && \
	seq -f "data/f5/file01_%04g.rgb" 40 47 | xargs touch && \
	seq -f "data/f5/file%g.03.rgb" 1 4 | xargs touch && \
	touch data/f5/alpha.txt && touch data/f5/file.info.03.rgb

fixtures: fixture1 fixture2 fixture3 fixture4 fixture5

site:
	mkdocs build && coverage html -d site/coverage/

all: clean init install fixtures test