import lss


def test_f3():
    result = lss.directory('data/f3')
    assert lss.text_lss(result) == '3 v001.%04d.txt\t1-3'


def test_f2_missing():
    result = lss.directory('data/f2.missing')
    assert lss.text_lss(result) == '1098 v001.%04d.txt\t1-1000 1003-1100'


def test_f1_4k():
    result = lss.directory('data/f1.4k')
    assert lss.text_lss(result) == '4000 v001.%04d.txt\t1-4000'


def test_f4():
    result = lss.directory('data/f4')
    assert lss.text_lss(result) == '1 elem.info\n46 sd_fx29.%04d.rgb\t101-121 123-147\n1 strange.xml'


def test_glob():
    result = lss.glob('data/f4', '*.rgb')
    assert lss.text_lss(result) == '46 data/f4/sd_fx29.%04d.rgb\t101-121 123-147'


def test_f5():
    result = lss.glob('data/f5', 'file*.03.rgb', default_factory=lss.FlexiblePathSequencer)
    assert lss.alnum_text_lss(result) == '4 data/f5/file%01d.03.rgb\t1-4\n1 data/f5/file.info.03.rgb'