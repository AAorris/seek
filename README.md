# lss

Typical `lss` module for listing sequences.

![](.coverage.svg)

[coverage report](https://lss.mor.now.sh/coverage/)

## Installation

`make install`

## Testing

Prepare test data automatically using

`make fixtures`

Test using

`make test`

Clean up using

`make clean`

## All at once

Do everything all at once with `make all`