"""The lss module - for listing sequences.

target a path, producing files
group files by sequence
present sequences
"""

import json
import os
from collections import defaultdict
from dataclasses import dataclass, field
from pathlib import Path
from typing import Any, Callable, Dict, Generic, Hashable, Iterable, TypeVar

from .presenters import alnum_text_lss, text_lss
from .sequence import (FlexiblePathSequencer, PathSequencer, SequencePair,
                       alnum_split, sequence_runs, simple_split)

__all__ = ("directory",)


def directory(dir, default_factory=PathSequencer) -> PathSequencer:
    """Dump sequences under a path to a string."""
    files = os.listdir(dir)
    return default_factory(files)


def glob(
    dir: str, pattern: str, recursive: bool = False, default_factory=PathSequencer
) -> PathSequencer:
    """Glob sequences under a path with a given pattern."""
    return default_factory(os.fspath(f) for f in Path(dir).glob(pattern))
