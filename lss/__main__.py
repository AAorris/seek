"""Main entrypoint for lss.

Usage:
    python -m lss --help

"""
import argparse
import logging
from pathlib import Path

import lss

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", action="store_true")
parser.add_argument("path", nargs="?", default=".")


def main():  # pragma: no cover
    """Lists directories, showing sequence information."""
    args = parser.parse_args()
    log_level = args.verbose and logging.DEBUG or logging.WARNING
    logging.basicConfig(level=log_level)
    print(lss.alnum_text_lss(lss.directory(args.path, lss.FlexiblePathSequencer)))


if __name__ == "__main__":
    main()  # pragma: no cover
