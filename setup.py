import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="seek",
    version="0.0.1",
    author="Aaron Morris",
    author_email="aaorris@gmail.com",
    description="Lists sequences.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/aaorris/seek",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    entry_points={
        'console_scripts': [
            'lss = lss.__main__:main'
        ]
    }
)