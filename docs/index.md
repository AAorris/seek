# LSS

API Documentation for LSS

## `__init__`

Get started with `import lss`

You may call `lss.directory(dir)` or `lss.glob(dir, pattern)`

Each can be passed a `default_factory` of a custom `PathSequencer` for extension.


## `__main__`

Stores the main program.

For test purposes, uses the `FlexiblePathSequencer` and the current directory by default.

Run with `python -m lss` or use `make setup` to install a dev executable and run `lss`.

## Sequence

Implements sequence functionality.

Sequences are simply compressed paths. They start with an iterable of files.

A common standard is for the last numeric segment before the final period is a frame sequence.

For this purpose, we implement `simple_path` to compress a path into a glob string, and
`simple_split` to compress a path into a padded sequence path and frame segment.
We encapsulate that into a `PathSequencer`, which takes a list of files and uses
dependency injection to accept overrides to the parsing functionality.

For the full flexibility required of lss, we offer an alternative, which splits paths into
alphabetical and numeric segments, compresses constant frame dimensions, and fills in a selected
dimension with frame runs, using `alnum_split`, `sequence_runs`, `frame_template`, and `FlexibleSequencer`.

## Presenters

Implements presentation functionality.

Presentation is a separate concern from sequence parsing.

From a path sequencer, `data_lss` treats sequenced paths by sorting values.
`json_lss` uses that for serializability and sorted output for tests and hashing.
`text_lss` uses `data_lss` to present `lss` style output as a string from a `PathSequencer`.
If using a `FlexibleSequencer` or `alnum_split`, `alnum_text_lss` will use `frame_template`
and `sequence_runs` to product `lss` style output.