"""Present lss data in different formats."""

import json
from itertools import cycle, islice
from typing import Dict, List, Tuple

from .sequence import CollectedSequence, frame_template, sequence_runs


def data_lss(lss) -> Dict:
    result = {}
    for key, frames in lss.group().items():
        result[key] = sorted(list(frames))
    return result


def _alnum_join(*iterables):
    """Round robin consume from iterables.

    Usage::
        >>> ''.join(_alnum_join(['img.', '.exr'], ['1001']))
        'img.1001.exr'
    """
    nexts = cycle(iter(it) for it in iterables)
    while True:
        try:
            for src in nexts:
                yield next(src)
        except StopIteration:
            return


def alnum_text_lss(lss) -> str:
    """Present an lss using alphanumeric splitting."""
    ret = {}
    for key, frame_tuples in data_lss(lss).items():
        axis, frames, template = frame_template(frame_tuples)
        runs = text_runs(sequence_runs(map(int, frames)))
        n = len(frames)
        if n == 1:
            template = [x if x is not None else frames[0] for x in template]
            path = ''.join(_alnum_join(key, template))
            ret[key] = f'1 {path}'
            continue
        template = [x if x is not None else f"%0{len(frames[0])}d" for x in template]
        path = ''.join(_alnum_join(key, template))
        ret[key] = f'{n} {path}\t{runs}'
    return "\n".join(ret[key] for key in sorted(ret))


def text_lss(lss) -> str:
    """Present an lss using simple splitting."""
    ret = {}
    for key, frames in data_lss(lss).items():
        n = len(frames)
        if n == 1:
            path = (key % int(frames[0])) if frames[0] is not None else key
            ret[key] = f"1 {path}"
            continue
        ranges = text_runs(sequence_runs(map(int, frames)))
        ret[key] = f"{n} {key}\t{ranges}"
    return "\n".join(ret[key] for key in sorted(ret))


def text_runs(runs: List[Tuple[int, int]]) -> str:
    """
    Examples::
        >>> text_runs([(1, 3), (2, 6)])
        '1-3 2-6'
    """
    return " ".join("-".join(map(str, pair)) for pair in runs)
