"""Sequence module with sequence related functions and classes."""

import re
from collections import defaultdict, namedtuple
from collections.abc import Set
from dataclasses import dataclass, field
from itertools import takewhile, tee
from pathlib import Path
from typing import (Any, Callable, Dict, Generator, Generic, Iterable, List,
                    Optional, Tuple)

# A path split into its generic and specific parts
SequencePair = namedtuple("SequencePair", ["path", "frame"])

# Paths grouped into its generic part and an iterable of frame parts
# Frames need to be grouped into runs, and frames may be multidimensionals
CollectedSequence = namedtuple("CollectedSequence", ["path", "frames"])


def simple_path(value: str) -> str:
    """Return a sequence string for this path.

    Where a path identifies a frame, a simple_path
    identifies the sequence as a whole.

    Example:
        >>> simple_path('/tmp/test_v1.comp.000101.1001.exr')
        '/tmp/test_v1.comp.000101.*.exr'
    """
    tmp = "".join(reversed(value))  # we'll match frames in reverse
    return "".join(reversed(re.sub(r"\.[0-9]+", ".*", tmp, 1)))


def simple_split(value: str) -> SequencePair:
    """Split a path into a sequence part and a frame part.

    Example:
        >>> simple_split('/tmp/test_v1.comp.000101.1001.exr')
        SequencePair(path='/tmp/test_v1.comp.000101.%04d.exr', frame='1001')
        >>> simple_split('/tmp/test.exr')
        SequencePair(path='/tmp/test.exr', frame=None)
    """
    backwards = lambda iterable: "".join(reversed(iterable))
    rev_value = backwards(value)  # we'll match frames in reverse
    match = re.search(r"\.([0-9]+)", rev_value)
    if match:
        frame_part = backwards(match.group(1))
        rev_padding = backwards(f"%0{len(frame_part)}d")
        simple_path = backwards(re.sub(r"\.[0-9]+", f".{rev_padding}", rev_value, 1))
        return SequencePair(simple_path, frame_part)
    return SequencePair(value, None)


@dataclass
class PathSequencer:
    """A file sequencer is for grouping paths into sequences."""

    files: Iterable
    create_pair: Callable = simple_split
    container_factory: Callable = lambda: defaultdict(set)

    @property
    def groups(self) -> Iterable[SequencePair]:
        for item in self.files:
            yield self.create_pair(item)

    def group(self) -> defaultdict:
        container = self.container_factory()
        for item in self.groups:
            container[item.path].add(item.frame)
        return container


def alnum_split(value: str) -> SequencePair:
    """Split a path into alphabetical and numeric parts.

    Example:
        >>> alnum_split('file1.03.rgb')
        SequencePair(path=('file', '.', '.rgb'), frame=('1', '03'))
    """
    return SequencePair(
        path=tuple(re.findall(r"[^0-9]+", value)),
        frame=tuple(re.findall(r"[0-9]+", value)),
    )


def sequence_runs(frames: Iterable[int]):
    """Get runs of a sequence.

    Examples:
        >>> sequence_runs({})
        []
        >>> sequence_runs({1,2,3})
        [(1, 3)]
        >>> sequence_runs({1,2,3,5})
        [(1, 3), (5, 5)]
        >>> sequence_runs({1,2,3,5,6})
        [(1, 3), (5, 6)]
    """
    ret: List[Tuple[int, int]] = []
    _frames = iter(frames)
    try:
        begin = next(_frames)
    except StopIteration:
        return ret
    prev = begin
    for cur in _frames:
        if cur != prev + 1:
            ret.append((begin, prev))
            begin = cur
        prev = cur
    ret.append((begin, prev))
    return ret


# For more flexible sequence parsing, we will take an approach where
# paths are separated into alpha and numeric tokens, and the paths are
# compared to each other. The first varying numeric token is assumed
# to be the frame component.


def frame_template(frames: List[Tuple]) -> Tuple[int, List[str], List[Optional[str]]]:
    """Given a collected sequence, return the best run.

    Collected sequences might have more than one numeric axis, so pick the one
    that varies over frames. Naively pick the first one that varies.
    Return the axis, frames, and template for unchanging values.
    The template uses a value of None to indicate the varying element.

    Example:
        >>> frame_template([('1', '03'), ('2', '03')])
        (0, ['1', '2'], [None, '03'])
        >>> frame_template([('1', '03'), ('1', '04')])
        (1, ['03', '04'], ['1', None])
        >>> frame_template([('1',), ('1',)])
        (0, ['1', '1'], [None])
    """
    template: List[Any] = list(frames[0])
    if len(frames) < 2:
        axis = len(frames[0]) - 1
        template[axis] = None
        return (axis, [k[-1] for k in frames], template)
    a, b = frames[:2]
    if len(a) < 2:
        template[0] = None
        return (0, [k[0] for k in frames], template)
    axis = 0
    while axis < len(a) and a[axis] == b[axis]:
        axis += 1
    template[axis] = None
    return axis, [k[axis] for k in frames], template


@dataclass
class FlexiblePathSequencer(PathSequencer):
    """A path sequencer for complex user paths.

    Uses less intuitive but more flexible methods of resolving
    sequences and so it is not provided as the default.
    
    """

    create_pair: Callable = alnum_split
